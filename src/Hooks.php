<?php


namespace calderawp\taco;
use calderawp\taco\edd\Logic as EDDLogic;
use calderawp\taco\edd\Subscription;
use calderawp\taco\edd\Subscriptions;
use calderawp\taco\UI\AppLogin;
use calderawp\taco\UI\ShowKeys;
use calderawp\taco\UI\SubscriptionChooser;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use calderawp\taco\ConvertKit\Logic as CKLogic;


/**
 * Class hooks
 * @package calderawp\taco
 */
class Hooks {

	/**
	 * Add hooks for interacting with EDD
	 */
	public function addHooks()
	{

		add_action( 'edd_subscription_post_create', [ $this, 'subscription_created' ] );
		add_action( 'edd_recurring_update_subscription', [ $this, 'subscription_updated' ] );
		add_action( 'edd_subscription_cancelled', [ $this, 'subscription_cancelled' ] );
		add_action( 'edd_subscription_receipt_after_table', [ $this, 'subscription_receipt'] );

		add_filter( 'the_content', [ $this, 'app_login' ] );
		add_filter( 'the_content', [ $this, 'subscription_chooser' ] );
		add_action( 'parse_request', [ $this, 'custom_endpoints' ] );

		add_action( 'rest_api_init', [ $this, 'rest_api_init' ] );

		add_filter( 'jwt_auth_token_before_dispatch', [ $this, 'jwt_return'], 10, 2 );

		add_action( 'edd_subscription_before_stats', [ $this, 'subscription_admin_view' ] );
	}


	/**
	 * When subscription is created, run our logic
	 *
	 * @uses "edd_subscription_post_create"
	 *
	 * @param int $id Subscription ID
	 */
	public function subscription_created( $id )
	{
		Create::subscription( $id );
	}

	/**
	 * When subscription is updated, maybe cancel or create or something in app
	 *
	 * @uses "edd_recurring_update_subscription"
	 *
	 * @param $id
	 */
	public function subscription_updated( $id )
	{

		$subscription = EDDLogic::factory( $id );
		if( $subscription && $subscription->statusActive() ){
			EDDLogic::maybeCreateRemote( $id );
			CKLogic::add( $subscription );
		} elseif( $subscription && $subscription->cancelStatus() ){
			EDDLogic::maybeCancelRemote( $id );
			$subscription = EDDLogic::factory( $id );
			if( $subscription ){
				CKLogic::remove( $subscription );
			}
		}
	}

	/**
	 * When subscription is canceled, run our logic
	 *
	 * @uses "edd_subscription_cancelled"
	 *
	 * @param int $id Subscription ID
	 */
	public function subscription_cancelled( $id )
	{
		EDDLogic::maybeCancelRemote( $id );
		$subscription = EDDLogic::factory( $id );
		if( $subscription ){
			CKLogic::remove( $subscription );
		}
	}



	/**
	 * In EDD receipt show the API keys
	 *
	 * @uses "edd_subscription_receipt_after_table" actiopn
	 *
	 * @param $payment
	 */
	public function subscription_receipt( $payment ){

		$args          = array(
			'parent_payment_id' => $payment->ID,
			'order'             => 'ASC'
		);
		$db            = new \EDD_Subscriptions_DB;

		$subscriptions = $db->get_subscriptions( $args );
		if(  empty( $subscriptions ) ){
			return;
		}

		foreach ( $subscriptions as $_subscription ) {
			$subscription = new Subscription( $_subscription  );
			if( ! $subscription->isSpace() ){
				continue;
			}
			$ui = new ShowKeys( $subscription );
			echo $ui->getHtml();

		}

	}

	/**
	 * Put app login form HTML on login page
	 *
	 * @uses "the_content"
	 *
	 * @param string $content
	 *
	 * @return string
	 */
	public function app_login( $content )
	{
		if( is_page( 'app-login' ) ){
			return ( new AppLogin() )->getHtml();
		}

		return $content;
	}

	/**
	 * On app post login page list subscription options
	 *
	 * @uses "the_content"
	 *
	 * @param $content
	 *
	 * @return string
	 */
	public function subscription_chooser( $content ){
		if( is_page( 'app-post-login' ) ){
			return ( new SubscriptionChooser() )->getHtml();
		}

		return $content;
	}

	/**
	 * Init Subscriptions WP API endpoint
	 *
	 * @uses "rest_api_init"
	 */
	public function rest_api_init( ){
		( new \calderawp\taco\WpApi\Subscriptions() )->addRoutes();
		( new \calderawp\taco\WpApi\Migrate() )->addRoutes();
	}

	/**
	 * Process custom endpoints
	 *
	 * @uses "parse_request"
	 *
	 * @param  \WP $wp
	 */
	public function custom_endpoints( $wp ){
		if( isset( $wp->query_vars[ 'pagename' ] ) ){
			$pagename =  $wp->query_vars[ 'pagename' ];
		}else{
			return;
		}

		if( in_array( $pagename, [
			'app-login',
			'app-post-login'
		] ) ){
			$request = Request::createFromGlobals();
			switch( $pagename ){

				case 'app-login':
					wp_logout();
					AuthToken::set( $request->query->get( 'cf-pro-auth', 0 ) );
					break;
				case 'app-post-login' :
					$token = AuthToken::get();
					$account = null;
					if( $request->get( 'account-choice', 0 ) && $request->get( '_wpnonce' ) ){
						if( wp_verify_nonce($request->get( '_wpnonce' ), 'app-post-login' ) ){
							$account = $request->get( 'account-choice' );
						}
					}
					$subscriptions = ( new Subscriptions( absint( get_current_user_id() ) ) )->getSubscriptions();
					if( empty( $subscriptions ) ){

						//redirect to product page
					}elseif ( 1 == count( $subscriptions ) ){
						$account = $subscriptions[0]->getSpaceId();
					}else{
						add_filter( 'the_title', function( $title, $id ){
							$post = get_post( $id );
							if( 'app-post-login' == $post->post_name ){
								return 'Choose Subscription';
							}
							return $title;
						}, 10 , 2 );
						//will let WordPress load page
					}

					if( $token && $account ){
						$response = new RedirectResponse( add_query_arg( [
							'token' => $token,
							'account' => $account
						], trailingslashit( Container::appUrl() ) . 'auth-return' ) );
						$response->send();
					}




					break;
			}
		}

	}

	/**
	 * Add user ID to JWT auth return
	 *
	 * @uses "jwt_auth_token_before_dispatch" filter
	 *
	 * @param $data
	 * @param \WP_User $user
	 *
	 * @return mixed
	 */
	public function jwt_return( $data, $user ){
		$data[ 'ID' ] = $user->ID;
		return $data;
	}

	/**
	 * Show API keys in EDD subscription UI
	 *
	 * Very quick and dirty, much admin who cares
	 *
	 * @var \EDD_Subscription
	 *
	 * @uses "edd_subscription_before_stats" action
	 */
	public function subscription_admin_view( $subscription ){
		if(  $subscription ){
			$subscription = new Subscription( $subscription );
			if( ! $subscription->isSpace() ){
				echo '<div>Not CF Pro</div>';
			}
			$id = $subscription->getSpaceId();
			$public = $subscription->getApiKeys()->getPublic();
			$private = $subscription->getApiKeys()->getPrivate();
			$token = $subscription->getApiKeys()->getToken();
			echo "<div><p>ID: $id</p><p>Public $public</p><p>Private $private</p><p>Token; $token</p></div>";
		}
	}

}