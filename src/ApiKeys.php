<?php


namespace calderawp\taco;
use calderawp\taco\edd\Subscription;


/**
 * Class ApiKeys
 * @package calderawp\taco
 */
class ApiKeys {

	protected $keys;
	/**
	 * @var Subscription
	 */
	protected $subscription;

	/**
	 * ApiKeys constructor.
	 *
	 * @param Subscription $subscription
	 */
	public function __construct( Subscription $subscription )
	{

		$this->subscription = $subscription;
		$this->getFromCache();
	}

	/**
	 * Get the public key
	 *
	 * @return string|bool
	 */
	public function getPublic()
	{
		return $this->getKey( 'public' );

	}

	/**
	 * Get the private key
	 *
	 * @return string|bool
	 */
	public function getPrivate()
	{
		return $this->getKey( 'private' );
	}

	/**
	 * Get CF Pro API token
	 *
	 * @return string
	 */
	public function getToken() : string
	{
		if( ! $this->getPublic() || ! $this->getPrivate() ){
			return '';
		}
		return sha1( $this->getPublic() . $this->getPrivate() );
	}

	/**
	 * @param string $key
	 *
	 * @return string|bool
	 */
	protected function getKey( string  $key )
	{
		if ( ! isset( $this->keys[ $key ] ) ) {
			$this->getFromApi();
		}

		if ( ! isset( $this->keys[ $key ] ) ) {
			return false;

		}

		return $this->keys[ $key ];
	}

	/**
	 * Get keys from the cache
	 */
	protected function getFromCache()
	{
		if( is_array( $keys = wp_cache_get( $this->cacheKey() ) ) ){
			$this->keys = $keys;
		}
	}




	/**
	 * Get keys from API
	 *
	 */
	protected function getFromApi()
	{
		if (  ! is_numeric( $this->subscription->getSpaceId() ) ) {
		}else{
			$api     = Container::getApiClient();
			$account = $api->getAccount( $this->subscription->getSpaceId() );
			if ( is_object( $account ) && ! is_wp_error( $account ) ) {
				$this->keys = [
					'public'  => $account->public_key,
					'private' => $account->private_key
				];
				wp_cache_set( $this->cacheKey(), $this->keys, 599 );
			}else{

			}
		}

	}


	protected function cacheKey()
	{
		return md5( __CLASS__ . TACO_VER  . $this->subscription->getId() );
	}
}