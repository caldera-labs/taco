<?php


namespace calderawp\taco;
use calderawp\convertKit\subscribers;


/**
 * Class init
 * @package calderawp\taco
 */
class Init {

	/**
	 * @var Hooks
	 */
	protected static $hooks;

	/**
	 * Start system
	 */
	public static function start()
	{
		self::$hooks = new Hooks();
		self::$hooks->addHooks();
	}

}