<?php


namespace calderawp\taco\edd;
use calderawp\taco\Container;


/**
 * Class SubscriptionQuery
 * @package calderawp\taco\edd
 */
class SubscriptionQuery {


	/**
	 * Get all subscriptions to CF Pro
	 *
	 * @return array
	 */
	public static function getAll() : array
	{
		return Container::getSubDB()->get_subscriptions( [
			'product_id' => SpaceIds::productIds(),
			'number'     => 999999
		] );
	}

	/**
	 * Get all subscriptions by product
	 *
	 * Useful to get subscriptions to old bundle
	 *
	 * @param int $productId
	 *
	 * @return array
	 */
	public static function byProduct( int $productId, int $total = null, int $page = 1 ) : array
	{
		if( ! $total ){
			$total = 999999;
		}
		if ( 1 !== $page ) {
			$offset = $total * $page;
		}else{
			$offset = 0;
		}
		return Container::getSubDB()->get_subscriptions( [
			'product_id' => $productId,
			'number'     => $total,
			'offset' => $offset
		] );

	}
}