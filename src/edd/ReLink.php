<?php


namespace calderawp\taco\edd;
use calderawp\taco\Api\AdminClient;
use calderawp\taco\Entity\Account;


/**
 * Class ReLink
 * @package calderawp\taco\edd
 */
class ReLink {

	/** @var Subscription  */
	protected $subscription;

	/** @var AdminClient  */
	protected $api;

	/** @var  Account */
	protected $account;

	/**
	 * ReLink constructor.
	 *
	 * @param Subscription $subscription
	 * @param AdminClient $api
	 */
	public function __construct( Subscription $subscription, AdminClient $api )
	{

		$this->subscription  = $subscription;
		$this->api = $api;
	}

	/**
	 * Find remote account
	 *
	 * @return bool
	 */
	public function findOnRemote() : bool
	{
		$account =$this->api->findByWpId( $this->subscription->getId() );
		if( is_object( $account ) && ! is_wp_error( $account ) ){
			$this->account = $account;
			return true;
		}

		return false;
	}

	/**
	 * Save ID of account
	 *
	 * @return bool
	 */
	public function writeId() : bool
	{
		if( is_object( $this->account ) ){
			$this->subscription->saveSpaceId( $this->account->getId() );
			return true;
		}

		return false;
	}

}