<?php


namespace calderawp\taco\edd;

use calderawp\taco\Container;


/**
 * Class Logic
 * @package taco\edd
 */
class Logic {

	/** @var  array */
	protected static $subscriptions;

	/**
	 * Attempt to create a subscription
	 *
	 * @param int $subscriptionId
	 *
	 * @return bool
	 */
	public static function maybeCreateRemote( $subscriptionId )
	{
		$subscription = self::factory( $subscriptionId );
		if( $subscription && $subscription->statusActive() ){

			$api = Container::getApiClient();
			$account = $api->findByWpId( $subscription->getId() );
			//If response is WP_Error doesn't exist
			if( ! is_wp_error( $account ) ){
				//@todo update
			}else{
				$account = $api->createAccount(
					$subscription->getId(),
					$subscription->getEmail(),
					$subscription->getName(),
					$subscription->getPlan()
				);

				if( ! is_wp_error( $account ) ){
					$subscription->saveSpaceId( $account->id );
					return true;
				}else{
					//API error, must report
				}
			}


		}

		return false;
	}

	/**
	 * Attempt to cancel a subscription
	 *
	 * @param $subscriptionId
	 *
	 * @return bool
	 */
	public static function maybeCancelRemote( $subscriptionId )
	{
		$subscription = self::factory( $subscriptionId );
		if( $subscription ){

			$api = Container::getApiClient();
			$account = $api->findByWpId( $subscription->getId() );
			if ( ! is_wp_error( $account ) ) {
				$api->updateAccountStatus( $account->id, false );
			}

			return false;

		}

	}

	/**
	 * Factory for subscriptions
	 *
	 * @param int $subscriptionId Subscription ID
	 *
	 * @return Subscription|null Returns Subscription onject is subscription exists and is Space
	 */
	public static function factory( $subscriptionId ){

		if( is_numeric( $subscriptionId ) ){
			if( ! isset( self::$subscriptions[ $subscriptionId ] ) ){
				self::$subscriptions[ $subscriptionId ] = new Subscription( new \EDD_Subscription($subscriptionId ) );
			}

		}else{
			return null;
		}


		if( self::$subscriptions[ $subscriptionId ]->isSpace() ){
			return self::$subscriptions[ $subscriptionId ];
		}

		return null;
	}

	/**
	 * Is a given product a space ID?
	 *
	 * @param int $productId
	 *
	 * @return bool
	 */
	public static function isSpace( int $productId ) : bool
	{
		return in_array( $productId, SpaceIds::productIds() );

	}


}