<?php


namespace calderawp\taco\edd;
use calderawp\taco\Container;


/**
 * Class SpaceIds
 * @package calderawp\taco\edd
 */
class SpaceIds {

	/**
	 * Get plan name by price ID
	 *
	 * @param int $priceId
	 *
	 * @return string
	 */
	public static function planByPriceId( int $priceId ): string
	{
		switch ( $priceId ){
			case 1:
			case 4:
				return 'basic';
				break;
			case 2:
			case 5:
				return 'awesome';
				break;
			case 3:
			case 6:
				return 'apex';
				break;
		}

		return 'basic';

	}

	/**
	 * Find plan name by product ID
	 *
	 * NOTE: Only works if is a bundle + pro
	 *
	 * @param int $productId
	 *
	 * @return string
	 */
	public static function planByProductId( int $productId ) : string
	{
		switch( $productId ) {
			case Container::getConfig()[ 'bundle_dev' ] :
				return 'basic';
				break;
			case Container::getConfig()[ 'bundle_agency' ] :
				return 'awesome';
				break;
			case  Container::getConfig()[ 'bundle_ent' ] :
				return 'apex';
				break;
		}

		return 'basic';
	}

	/**
	 * IDs of space product

	 *
	 * @return array
	 */
	public static function productIds() : array
	{
		$config = Container::getConfig();


		return [
			Container::spaceId(),
			$config[ 'bundle_dev' ],
			$config[ 'bundle_agency' ],
			$config[ 'bundle_ent' ],
		];

	}


}