<?php


namespace calderawp\taco\edd;
use calderawp\taco\Container;


/**
 * Class Subscriptions
 * @package calderawp\taco\edd
 */
class Subscriptions {

	/** @var  array */
	protected $subscriptions;

	/** @var \EDD_Recurring_Subscriber  */
	protected $subscriber;

	protected $count;

	public function __construct( int $user_id  )
	{
		$this->subscriber = new \EDD_Recurring_Subscriber( $user_id, true );
		$this->find();
	}

	/**
	 * Get all space subscriptions as our Subscription object
	 *
	 * @return array
	 */
	public function getSubscriptions() : array
	{
		$subscriptions = [];
		if( ! empty( $this->subscriptions ) ){
			foreach ( $this->subscriptions as $subscription ) {
				$subscriptions[] = new Subscription( $subscription );
				
			}
		}

		$this->count = count( $subscriptions );

		return $subscriptions;
	}

	/**
	 * Get total number of subscriptions
	 *
	 * @return int|null
	 */
	public function getCount()
	{
		return $this->count;
	}

	public function toArray() : array
	{
		$array = [];
		foreach ( $this->getSubscriptions() as $subscription ){
			$array[] = $subscription->toArray();
		}
		return $array;
	}

	/**
	 * Find all space subscriptions
	 */
	protected function find()
	{
		$this->subscriptions =  $this->subscriber->get_subscriptions( SpaceIds::productIds() );
		
	}
}

