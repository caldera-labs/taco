<?php


namespace calderawp\taco\edd;
use calderawp\taco\ApiKeys;
use calderawp\taco\Container;


/**
 * Class Subscription
 * @package taco\edd
 */
class Subscription {
	/** @var \EDD_Subscription  */
	private $subscription;

	/** @var int  */
	protected $id;

	/**
	 * @var ApiKeys
	 */
	private  $apiKeys;
	/**
	 * Subscription constructor.
	 *
	 * @param \EDD_Subscription $subscription
	 */
	public function __construct( \EDD_Subscription $subscription )
	{
		$this->subscription = $subscription;
		$this->id = intval( $this->subscription->id );

	}

	public function toArray()
	{
		return [
			'id' => $this->id,
			'active' => $this->statusActive(),
			'created' => $this->subscription->created,
			'expires' => $this->subscription->expiration,
			'plan' => $this->getPlan(),
			'status_label' => $this->subscription->get_status_label(),
			'public_key' => $this->getApiKeys()->getPublic(),
			'private_key' => $this->getApiKeys()->getPrivate(),
			'token'     => $this->getApiKeys()->getToken()
		];
	}
	/**
	 * Is this a space subscription?
	 *
	 * @return bool
	 */
	public function isSpace() : bool
	{
		return  Logic::isSpace( (int) $this->subscription->product_id );
	}

	/**
	 * Get ID of account in CF Pro app
	 *
	 * @return int|null
	 */
	public function getSpaceId()
	{

		$id = $this->findSpaceId();
		if( $id ){
			return $id;
		}
		$this->reLink();
		$id = $this->findSpaceId();
		if( $id ){
			return $id;
		}

		return null;

	}

	/**
	 * Find saved CF Pro account ID
	 *
	 * @return int|null
	 */
	protected function findSpaceId()
	{
		$ids = $this->getSpaceIds();
		if( array_key_exists( $this->id, $ids ) ) {
			return absint( $ids[ $this->id ] );
		}
		return null;
	}

	/**
	 * Check if subscription has a status that means it should be active in Caldera Forms Pro
	 *
	 * @return bool
	 */
	public function statusActive() : bool
	{
		return in_array( $this->subscription->get_status(), [
			'active',
			'trialling'
		]);

	}

	/**
	 * Check if subscription has a status that indicates
	 *
	 * @return bool
	 */
	public function cancelStatus() : bool
	{
		if( 'pending' == $this->subscription->get_status() ){
			return true;
		}

		return ! $this->statusActive();
	}

	public function getPlan()
	{
		$payment = $this->getPayment();
		$downloads = $payment->downloads;
		$plan = '';
		if( ! empty( $downloads ) ) {
			foreach ( $downloads as $download ) {
				if( $download[ 'id' ] ==  Container::spaceId() ){
					$plan = SpaceIds::planByPriceId( absint( $download[ 'options' ][ 'price_id' ] ) );
					break;
				}

				if( Logic::isSpace( $download[ 'id' ] ) ){
					$plan = SpaceIds::planByProductId( (int) $download[ 'id' ] );
				}

			}



		}

		if( empty( $plan ) ){
			$plan = SpaceIds::planByPriceId( 0 );
		}
		return $plan;
	}

	/**
	 * Cancel plan (locally, doesn't cancel in app)
	 */
	public function cancel()
	{
		$this->subscription->cancel();
	}

	/**
	 * Save ID of  account in CF Pro App
	 *
	 * @param int $id
	 */
	public function saveSpaceId(  $id ){
		$ids = $this->getSpaceIds();
		if( ! array_key_exists( $this->id, $ids ) ) {
			$ids[ $this->id ] = $id;
		}
		$this->getCustomer()->update_meta( '_space_subscriptions', $ids );

	}

	/**
	 * @return array|mixed
	 */
	public function getSpaceIds()
	{
		$ids = $this->getCustomer()->get_meta( '_space_subscriptions' );
		if( empty( $ids ) ){
			return [];
		}
		return $ids;
	}

	/**
	 * Get ID of EDD subscription
	 *
	 * @return int
	 */
	public function getId() : int
	{
		return absint( $this->id );
	}

	/**
	 * Get user ID for this subscription
	 *
	 * @return int
	 */
	public function getUserId() : int
	{
		return absint( $this->getCustomer()->user_id );
	}

	/**
	 * Get EDD_Customer object for this subscription
	 *
	 * @return \EDD_Customer
	 */
	protected function getCustomer() : \EDD_Customer
	{
		return $this->subscription->customer;
	}

	/**
	 * Get customer's name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->getCustomer()->name;
	}

	/**
	 * Get customer's email
	 *
	 * @return string
	 */
	public function getEmail()
	{
		return $this->getCustomer()->email;
	}

	/**
	 * Get subscription total uses
	 *
	 * @return int
	 */
	public function getUses() : int
	{
		return 5000;
	}

	protected function getPayment() : \EDD_Payment
	{
		return new \EDD_Payment( $this->subscription->parent_payment_id );
	}

	/**
	 * @return ApiKeys
	 */
	public function getApiKeys()
	{
		if( ! $this->apiKeys ){
			$this->apiKeys = new ApiKeys( $this );
		}

		return $this->apiKeys;
	}

	/**
	 * Attempt to relink account
	 *
	 * @return bool
	 */
	public function reLink()
	{
		$relink = new ReLink( $this, Container::getApiClient() );
		if( $relink->findOnRemote() ){
			return $relink->writeId();
		}
		return false;
	}

}