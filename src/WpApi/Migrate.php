<?php


namespace calderawp\taco\WpApi;
use calderawp\taco\ConvertKit\AddKeys;
use calderawp\taco\Create;
use calderawp\taco\edd\Logic;
use calderawp\taco\edd\SpaceIds;
use calderawp\taco\edd\SubscriptionQuery;


/**
 * Class Migrate
 * @package calderawp\taco\WpApi
 */
class Migrate {

	use EDDAuth;


	/**
	 * Add routes
	 */
	public function addRoutes()
	{
		register_rest_route( 'taco/v1', '/migrate/subscription/(?P<id>[\w-]+)', [
			'methods'             => 'POST',
			'permission_callback' => [ $this, 'authPermission' ],
			'callback'            => [ $this, 'migrateSubscription' ]
		] );

		register_rest_route( 'taco/v1', '/migrate/product/(?P<id>[\w-]+)', [
			'methods'             => 'POST',
			'permission_callback' => [ $this, 'authPermission' ],
			'callback'            => [ $this, 'migrateProduct' ],
			'args' => [
				'page' => [
					'sanitize_callback' => 'absint',
					'default' => 1
				]
			]
		] );

		register_rest_route( 'taco/v1', '/migrate/ck/(?P<id>[\w-]+)', [
			'methods'             => 'POST',
			'permission_callback' => [ $this, 'authPermission' ],
			'callback'            => [ $this, 'ckKeys' ]
		] );


	}

	/**
	 * Migrate one subscription to CF Pro
	 *
	 * POST /taco/v1/migrate/subscription/<sub-id>
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function migrateSubscription( \WP_REST_Request $request )
	{
		if( is_numeric( $request[ 'id' ] ) ){
			$subscription = Create::subscription( $request[ 'id' ] );
			if( ! $subscription ){
				return new \WP_Error( 'Subscription not created' );
			}
			return new \WP_REST_Response( $subscription->toArray(), 201  );
		}

		return new \WP_Error( 'Invalid' );
	}

	/**
	 * Migrate all subscription of one bundle product to CF Pro
	 *
	 * POST /taco/v1/migrate/product/<product-id>
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function migrateProduct( \WP_REST_Request $request )
	{
		if ( ! is_numeric( $request[ 'id' ] ) || ! Logic::isSpace( $request[ 'id' ] ) ) {
			return new \WP_Error( 'Invalid' );
		}

		$data = [
			'created' => [],
			'skipped' => []
		];
		$subscriptions = SubscriptionQuery::byProduct( $request[ 'id' ], 5, (int) $request[ 'page' ] );
		foreach ( $subscriptions as $subscription ){
			$created = Create::subscription( (int) $subscription->id );
			if ( $created ) {
				$data[ 'created' ][$subscription->id] = $created;
			} else {
				$data[ 'skipped' ][$subscription->id] = $subscription->id;
			}
		}

		return rest_ensure_response( $data );


	}

	/**
	 * Update API Key custom fields in ConvertKit
	 *
	 * POST /taco/v1/migrate/ck/(sub-id)/ck
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return mixed|\WP_Error|\WP_REST_Response
	 */
	public function ckKeys( \WP_REST_Request $request )
	{
		if( ! is_numeric( $request[ 'id' ] ) ){
			if ( ! is_numeric( $request[ 'id' ] ) ) {
				return new \WP_Error( 'Invalid' );
			}
		}

		$subscription = Logic::factory( $request[ 'id' ] );
		if( ! $subscription ){
			return new \WP_Error( 'Not Found' );
		}

		$updated = ( new AddKeys( $subscription ) )->tryToAdd();
		if( $updated ){
			return rest_ensure_response( 'Updated :)' );
		}

		return new \WP_Error( 'Not Updated :(' );

	}

}