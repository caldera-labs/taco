<?php


namespace calderawp\taco\WpApi;


trait EDDAuth {


	/**
	 * Check auth based on EDD API
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return bool|\WP_Error
	 */
	protected function checkAuth( \WP_REST_Request $request )
	{


		$public =  ! empty( $request->get_header( 'PHP_AUTH_USER' ) ) ? $request->get_header( 'PHP_AUTH_USER' ) : $request->get_param( 'key' );
		$user = EDD()->api->get_user( $public );
		if ( $user  ) {
			$token = ! empty(  $request->get_header( 'PHP_AUTH_PW' ) ) ? $request->get_header( 'PHP_AUTH_PW' ) : $request->get_param( 'token' );
			$secret = EDD()->api->get_user_secret_key( $user );

			if( $this->checkKeys( $secret, $public, $token ) ){
				return $user;
			}
		}

		return new \WP_Error( 400, 'Not Authorized' );

	}

	/**
	 * Check API keys vs token
	 *
	 * @param string $secret Secret key
	 * @param string $public Public key
	 * @param string $token Token used in API request
	 *
	 *  @return bool
	 */
	protected function checkKeys( $secret, $public, $token ){
		//@todo update for https://github.com/easydigitaldownloads/easy-digital-downloads/issues/5853
		if ( hash_equals( md5( $secret . $public ), $token ) ) {
			return true;
		} else {
			return false;
		}

	}


	/**
	 * Check that user is logged in
	 *
	 * @return bool
	 */
	public function permission( \WP_REST_Request $request )
	{
		if( ! get_current_user_id() ){
			$user = $this->checkAuth( $request );
			if( ! is_wp_error ( $user ) ){
				wp_set_current_user( $user );

			}else{
				return $user;
			}
		}


		return 0 != get_current_user_id();
	}

	/**
	 * Check that user is auth
	 *
	 * @return bool
	 */
	public function authPermission( \WP_REST_Request $request )
	{
		//do EDD auth
		$this->permission( $request );
		return current_user_can( 'manage_options' );
	}



}