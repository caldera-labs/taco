<?php


namespace calderawp\taco\WpApi;
use calderawp\taco\Container;
use calderawp\taco\edd\SpaceIds;
use calderawp\taco\edd\Subscription;
use calderawp\taco\edd\SubscriptionQuery;
use calderawp\taco\edd\Subscriptions as SubscriptionsCollection;
use calderawp\taco\edd\Logic as EDDLogic;
/**
 * Class Accounts
 * @package calderawp\taco\WpApi
 */
class Subscriptions {
	use EDDAuth;

	/**
	 * Add routes
	 */
	public function addRoutes()
	{
		register_rest_route( 'taco/v1', '/subscriptions',[
				'methods' => 'GET',
				'permission_callback' => [ $this, 'permission' ],
				'callback' => [ $this, 'getSubscriptions']
			]
		);

		register_rest_route( 'taco/v1', '/product/ids',[
				'methods' => 'GET',
				'permission_callback' => [ $this, 'authPermission' ],
				'callback' => [ $this, 'productIds']
			]
		);

		register_rest_route( 'taco/v1', '/product/details',[
				'methods' => 'GET',
				'permission_callback' => [ $this, 'authPermission' ],
				'callback' => [ $this, 'productdetails']
			]
		);

		register_rest_route( 'taco/v1', '/subscriptions/simple',[
				'methods' => 'GET',
				'permission_callback' => [ $this, 'authPermission' ],
				'callback' => [ $this, 'simpleSubscriptions']
			]
		);

		register_rest_route( 'taco/v1', '/subscriptions/customer',[
				'methods' => 'GET',
				'permission_callback' => [ $this, 'authPermission' ],
				'callback' => [ $this, 'getCustomer'],
				'args' => [
					'email' => [
						'required' => true
					]
				]
			]
		);

		register_rest_route( 'taco/v1', '/subscriptions/(?P<id>[\w-]+)',[
				'methods' => 'GET',
				'permission_callback' => [ $this, 'permission' ],
				'callback' => [ $this, 'getSubscription'],
				'args' => [
					'user' => [
						'required' => false,
						'sanitization_callback' => 'absint'
					]
				]
			]
		);
		register_rest_route( 'taco/v1', '/subscriptions/(?P<id>[\w-]+)',[
				'methods' => 'POST',
				'permission_callback' => [ $this, 'authPermission' ],
				'callback' => [ $this, 'updateSubscription'],
				'args' => [
					'cancel' => [
						'required' => false,
						'default' => false
					],
				]
			]
		);
	}

	/**
	 * Check that user is logged in
	 *
	 * @return bool
	 */
	public function permission( \WP_REST_Request $request )
	{
		if( ! get_current_user_id() ){
			$user = $this->checkAuth( $request );
			if( ! is_wp_error ( $user ) ){
				wp_set_current_user( $user );

			}else{
				return $user;
			}
		}


		return 0 != get_current_user_id();
	}

	/**
	 * Check that user is auth
	 *
	 * @return bool
	 */
	public function authPermission( \WP_REST_Request $request )
	{
		//do EDD auth
		$this->permission( $request );
		return current_user_can( 'manage_options' );
	}

	/**
	 * Get subscription details for one subscription
	 *
	 * GET /taco/v1/subscriptions
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return mixed|\WP_REST_Response
	 */
	public function getSubscription( \WP_REST_Request $request )
	{
		$subscription = EDDLogic::factory( $request[ 'id' ] );

		if( $subscription ){
			if( current_user_can( 'manage_options' ) || $subscription->getUserId() === get_current_user_id() ) {


				return rest_ensure_response( $subscription->toArray() );
			}

			return rest_ensure_response( new \WP_Error( 400, 'Unauthorized' ) );
		}

	}

	/**
	 * Update one subscription
	 *
	 * POST /taco/v1/subscriptions/<id>
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return mixed|\WP_REST_Response
	 */
	public function updateSubscription( \WP_REST_Request $request )
	{
		$subscription = EDDLogic::factory( $request[ 'id' ] );

		if( $subscription ){
			if ( ! current_user_can( 'manage_options' ) ) {
				if ( $subscription->getUserId() !== get_current_user_id() ) {
					return rest_ensure_response( new \WP_Error( 400, 'Unauthorized' ) );
				}
			}

			if( $request[ 'cancel' ] ){
				$subscription->cancel();
			}

			return rest_ensure_response( $subscription->toArray() );
		}
	}

	/**
	 * Get subscription details for all subscriptions of current users
	 *
	 * GET /taco/v1/subscriptions
	 *
	 * @return mixed|\WP_REST_Response
	 */
	public function getSubscriptions( \WP_REST_Request $request )
	{
		if( is_numeric( $user = $request->get_param( 'user' )   ) ){
			if( $user != get_current_user_id() || ! current_user_can( 'manage_options' ) ){
				$user = get_current_user_id();
			}else{

			}

		}else{
			$user = get_current_user_id();
		}

		$subscriptions = new SubscriptionsCollection( $user );
		return $this->createResponse( $subscriptions->toArray(), $subscriptions->getCount() );
	}

	/**
	 *
	 * GET taco/v1/subscriptions/simple
	 *
	 * @return mixed|\WP_REST_Response
	 */
	public function simpleSubscriptions()
	{
		$subscriptions = SubscriptionQuery::getAll();


		$data = [];
		if( ! empty( $subscriptions ) ){
			/** @var \EDD_Subscription $subscription */
			foreach ( $subscriptions as $subscription ){
				$sub = new Subscription( $subscription );
				if( ! $sub->getSpaceId() ){
					continue;
				}
				$data[] = [
					'email'     => $subscription->customer->email,
					'wp_id'        => $subscription->id,
					'id' => $sub->getSpaceId()
				];
			}

		}


		return $this->createResponse( $data, count( $subscriptions ) );
	}

	/**
	 * List product IDs that are CF Pro or bundles with CF Pro
	 *
	 * GET /products/ids
	 *
	 * @return mixed|\WP_REST_Response
	 */
	public function productIds()
	{
		return rest_ensure_response( SpaceIds::productIds() );
	}

	/**
	 * List product IDs that are CF Pro or bundles with CF Pro
	 *
	 * GET /products/details
	 *
	 * @return mixed|\WP_REST_Response
	 */
	public function productDetails()
	{
		$data = [];
		foreach ( SpaceIds::productIds() as $id ){
			$download = new \EDD_Download( $id );
			$data[] = [
				'id' => $id,
				'name' => $download->post_title,
				'price' => $download->get_price()
			];

		}

		return $this->createResponse( $data, count( $data ) );
	}

	/**
	 * Get a customer's subscriptions
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function  getCustomer( \WP_REST_Request $request )
	{
		if( ! is_email( $request[ 'email'] )) {
			return new \WP_Error( 'invalid email' );
		}

		$user = get_user_by( 'email', $request[ 'email' ] );
		if( ! $user ){
			return new \WP_Error( 'User not found' );

		}

		$subscriptions = new SubscriptionsCollection( $user->ID );

		return $this->createResponse( $subscriptions->toArray(), $subscriptions->getCount() );


	}

	/**
	 * @param array $data
	 * @param $total
	 *
	 * @return \WP_REST_Response
	 */
	protected function createResponse( array $data, $total ) : \WP_REST_Response
	{
		$response = new \WP_REST_Response( $data );
		$response->header( 'X-TACO-TOTAL', $total );
		return $response;
	}


}