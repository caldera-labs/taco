<?php


namespace calderawp\taco\Entity;
use calderawp\taco\edd\Subscription;


/**
 * Class Subscriber
 * @package calderawp\taco
 */
class Subscriber  extends Entity{

	protected $name;

	protected $email;

	protected $userId;

	protected $spaceId;

	public function toArray() : array
	{
		return [
			'wp_id' => $this->userId,
			'default_reply' => [
				'name' => $this->name,
				'email' => $this->email
			]
		];
	}

	/**
	 * Create entity from bigger subcriber object
	 *
	 * @param Subscription $subscription
	 *
	 * @return Subscriber
	 */
	public static function fromEDD( Subscription $subscription ) : Subscriber
	{

		$obj = new static();
		$obj->userId = $subscription->getUserId();
		$obj->name = $subscription->getName();
		$obj->email = $subscription->getEmail();
		$obj->spaceId = $subscription->getSpaceId();
		return $obj;
	}

}