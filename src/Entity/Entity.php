<?php


namespace calderawp\taco\Entity;
use Illuminate\Config\Repository;


/**
 * Class Entity
 * @package calderawp\taco
 */
abstract class Entity {
	/**
	 * @inheritdoc
	 */
	public function __set( $name, $value )
	{
		if( property_exists( $this, $name ) ){
			$this->$name = $value;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function __get( $name )
	{

		if( property_exists( $this, $name ) ){
			return $this->name;
		}

	}

	/**
	 * Convert to array
	 *
	 * @return array
	 */
	abstract public function toArray() : array;

}