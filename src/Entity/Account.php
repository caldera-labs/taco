<?php


namespace calderawp\taco\Entity;



/**
 * Class Account
 * @package calderawp\taco\Entity
 */
class Account  {

	/** @var  int */
	protected $wp_id;

	/** @var  string */
	protected $public_key;

	/** @var  string */
	protected $private_key;

	/** @var  int */
	protected $id;

	/** @var  bool */
	protected $active;

	/** @var  string */
	protected $plan;

	public function __construct( \stdClass $object )
	{
		if ( isset( $object->wp_id ) ) {
			$this->wp_id = $object->wp_id;
		}

		$this->public_key = $object->public_key;
		$this->private_key = $object->private_key;
		$this->plan = $object->plan;
		$this->active = $object->active;
		$this->id = $object->id;
	}

	public function __get( $name )
	{
		if( isset( $this->$name ) ){
			return $this->$name;
		}
	}

	/**
	 * Get Caldera Forms Pro account Id
	 *
	 * @return int
	 */
	public function getId() : int
	{
		return (int) $this->id;
	}
}