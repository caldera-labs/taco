<?php


namespace calderawp\taco\UI;
use calderawp\taco\Container;


/**
 * Class AppLogin
 * @package calderawp\taco\UI
 */
class AppLogin implements View{

	/** @var  string */
	protected $token;

	public function __construct( $token =  '' )
	{
		if( ! $token && isset( $_GET[ 'cf-pro-auth' ]) ){
			$token = $_GET[ 'cf-pro-auth' ];
		}
		$this->token = $token;
	}

	public function getHtml() : string
	{
		if( empty( $this->token ) ){
			return 'Token missing. Please try again from app';
		}

		if( get_current_user_id() ){
			wp_safe_redirect( home_url( 'app-post-login' ) );
		}

		$pre = sprintf( '<div id="taco-terms">By using Caldera Forms Pro, you agree to abide by the <a href="%s" target="_blank">terms of services</a></div>', home_url( 'pro/terms-of-service/' ) );
		$args = array(
			'echo'           => false,
			'remember'       => true,
			'redirect'       => home_url( 'app-post-login' ),
			'form_id'        => 'taco-app-login',
			'id_username'    => 'user_login',
			'id_password'    => 'user_pass',
			'id_remember'    => 'rememberme',
			'id_submit'      => 'taco-app-login-submit',
			'label_username' => __( 'Username' ),
			'label_password' => __( 'Password' ),
			'label_remember' => __( 'Remember Me' ),
			'label_log_in'   => __( 'Log In To Caldera Forms Pro' ),
			'value_username' => '',
			'value_remember' => false
		);

		return $pre . wp_login_form( $args );
	}

}