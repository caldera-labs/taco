<?php


namespace calderawp\taco\UI;
use calderawp\taco\ApiKeys;
use calderawp\taco\edd\Subscription;


/**
 * Class ShowKeys
 * @package calderawp\taco\UI
 */
class ShowKeys implements View{

	/** @var  Subscription */
	protected $subscription;
	/** @var ApiKeys  */
	protected $keys;

	/**
	 * ShowKeys constructor.
	 *
	 * @param Subscription $subscription
	 */
	public function __construct( Subscription $subscription )
	{
		$this->subscription = $subscription;
		$this->keys = new ApiKeys( $this->subscription );
	}

	/** @inheritdoc */
	public function getHtml( ) : string
	{
		$public = $this->keys->getPublic();
		$private = $this->keys->getPrivate();
		ob_start();
		?>
			<div class="taco-keys ">
				<h3>
					<?php esc_html_e( 'Subscription Details', 'taco' ); ?>
				</h3>
				<table>
					<thead>
						<tr>
							<th>
								<?php esc_html_e( 'Public Key', 'taco' ); ?>
							</th>
							<th>
								<?php esc_html_e( 'Secret Key', 'taco' ); ?>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<?php echo $public; ?>
							</td>
							<td>
								<?php echo $private; ?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

		<?php
		return ob_get_clean();

	}

}