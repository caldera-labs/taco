<?php


namespace calderawp\taco\UI;


interface View {

	/**
	 * Get the view's HTML
	 *
	 * @return string
	 */
	public function getHtml() : string;
}