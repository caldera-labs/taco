<?php


namespace calderawp\taco\UI;
use calderawp\taco\ApiKeys;
use calderawp\taco\edd\Subscription;
use calderawp\taco\edd\Subscriptions;


/**
 * Class SubscriptionList
 * @package calderawp\taco\UI
 */
class SubscriptionChooser implements View {

	/** @inheritdoc */
	public function getHtml() : string
	{

		$out = '';
		$subscriptions = $this->getSubscriptions();
		if( empty( $subscriptions ) ){
			$out .= '<p>No Subscriptions Found</p>';
		}
		$out .= '<form action="' . home_url( 'app-post-login' ) . '" method="GET" id="taco-account-chooser" class="taco-form">';
		$out .= '<div role="field" class="form-group"> <label for="account-choice" class="control-label">' . esc_html__( 'Choose Subscription', 'taco' ) . '</label><select id="account-choice" name="account-choice" class="form-control" >';
		/** @var Subscription $subscription */
		foreach ( $subscriptions as $subscription ){
			$id = $subscription->getSpaceId();
			$key = ( new ApiKeys( $subscription ) )->getPublic();
			if (  $key ) {
				$out .=
					'<option value=' . esc_attr( $id ) . '>' . $key . '</option>';
			}
		}
		$out .= '</select></div>';
		$out .= wp_nonce_field( 'app-post-login', '_wpnonce', false, false );
		$out .= '<input type="submit" value="Choose" class="btn button btn-block btn-primary " />';
		$out .= '</form>';
		return $out;
	}

	/**
	 * @return mixed
	 */
	protected function getSubscriptions()
	{
		$subscriptions = ( new Subscriptions( absint( get_current_user_id() ) ) )->getSubscriptions();

		return $subscriptions;
	}
}