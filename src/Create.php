<?php


namespace calderawp\taco;

use calderawp\taco\edd\Logic as EDDLogic;
use calderawp\taco\ConvertKit\Logic as CKLogic;
use calderawp\taco\edd\Subscription;

/**
 * Class Create
 * @package calderawp\taco
 */
class Create {

	/**
	 * Create subscription on CF Pro app
	 *
	 * @param $id
	 *
	 * @return bool|Subscription|null
	 */
	public static function subscription( $id, $checkFirst = true )
	{
		if( $checkFirst ){
			$subscription = EDDLogic::factory( $id );
			if( $subscription ){
				if ( $subscription->getSpaceId() ) {
					return $subscription;
				}

			}

		}

		EDDLogic::maybeCreateRemote( $id );
		$subscription = EDDLogic::factory( $id );
		if( $subscription && $subscription->statusActive() ){
			EDDLogic::maybeCreateRemote( $id );
			CKLogic::add( $subscription );

			return $subscription;
		}

		return false;
	}

}