<?php


namespace calderawp\taco\ConvertKit;
use calderawp\convertKit\tags;
use calderawp\taco\ApiKeys;
use calderawp\taco\Container;
use calderawp\taco\edd\Subscription;


/**
 * Class Client
 * @package calderawp\taco\Subscribe
 */
abstract class Tagger {

	use data;

	/** @var  tags */
	protected $client;

	/**
	 * @var array
	 */
	protected $ck_tags = [
		'basic'   => 230414,
		'apex'    => 230419,
		'awesome' => 230418,
	];


	/**
	 * Tagger constructor.
	 *
	 * @param Subscription $subscription
	 */
	public function __construct( Subscription $subscription )
	{
		$this->subscription = $subscription;
		$this->createClient();
		$this->setApiKeys();
	}

	/**
	 * Create tags API client
	 */
	protected function createClient()
	{
		$this->client = new tags( Container::getConfigItem( 'ck_public' ), Container::getConfigItem( 'ck_secret' ) );

	}


	/**
	 * Find the correct CK tag ID
	 *
	 * @return mixed
	 */
	protected function findTag()
	{
		$plan = $this->subscription->getPlan();
		return $this->ck_tags[ $plan ];
	}

	/**
	 * Make the API request
	 *
	 * @return mixed
	 */
	abstract public function makeRequest();


	/**
	 * Find ConvertKit subscriber by EDD subscription email
	 *
	 * @return bool
	 */
	public function findSubscriber()
	{
		$email = $this->subscription->getEmail();
		$url = add_query_arg(
			[
				'api_key' => 	\calderawp\taco\Container::getConfigItem( 'ck_public' ),
				'api_secret' => \calderawp\taco\Container::getConfigItem( 'ck_secret' ),
				'email_address' => urlencode( $email )
			],
			'https://api.convertkit.com/v3/subscribers'
		);

		$r = wp_remote_get( $url );
		if ( ! is_wp_error( $r ) ) {
			$body = json_decode( wp_remote_retrieve_body( $r ) );
			if( isset( $body->subscribers ) ){
				foreach ( $body->subscribers as $subscriber ){
					if( $email === $subscriber->email_address ){
						return $subscriber;

					}
				}
			}

		}

		return false;

	}

}