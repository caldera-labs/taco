<?php


namespace calderawp\taco\ConvertKit;


use calderawp\taco\ApiKeys;
use calderawp\taco\edd\Subscription;

trait data {

	/** @var  APIKeys */
	protected $apiKeys;

	/** @var Subscription  */
	protected $subscription;


	protected function subscriberData() : array
	{
		return [
			'first_name' => $this->subscription->getName(),
			'fields' => [
				'user_id' => $this->subscription->getUserId(),
				'cf_pro_public' => $this->apiKeys->getPublic(),
				'cf_pro_secret' => $this->apiKeys->getPrivate(),
			]

		];
	}

	public function setApiKeys()
	{
		$this->apiKeys = new ApiKeys( $this->subscription );
	}
}