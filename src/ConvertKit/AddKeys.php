<?php


namespace calderawp\taco\ConvertKit;
use calderawp\convertKit\subscribers;
use calderawp\taco\ApiKeys;
use calderawp\taco\Container;
use calderawp\taco\edd\Subscription;


/**
 * Class AddKeys
 * @package calderawp\taco\ConvertKit
 */
class AddKeys {

	use data;

	/** @var  APIKeys */
	protected $apiKeys;

	/** @var Subscription  */
	protected $subscription;

	/**
	 * @var subscribers
	 */
	protected $client;

	/**
	 * AddKeys constructor.
	 *
	 * @param Subscription $subscription
	 * @param \APIKeys|null $apiKeys
	 */
	public function __construct( Subscription $subscription, ApiKeys $apiKeys = null )
	{
		$this->subscription = $subscription;
		if( ! $apiKeys ){
			$this->setApiKeys();
		}else{
			$this->apiKeys = $apiKeys;
		}

		$this->client = new subscribers( Container::getConfigItem( 'ck_public' ), Container::getConfigItem( 'ck_secret' ) );
	}

	/**
	 * Try to update subscriber with API keys
	 *
	 * @param bool $recurse Optional. If true, tagger will run the and then this will run again once. False to prevent 2nd recursion.
	 *
	 * @return bool|object|string
	 */
	public function tryToAdd( $recurse = true )
	{
		$id = $this->find();
		if( $id ){
			return $this->update( $id );
		}else{
			if ( $recurse ) {
				Logic::add( $this->subscription );
				$this->tryToAdd( false );
			}
		}

		return false;

	}

	/**
	 * Find ConvertKit subscriber ID
	 *
	 * @return int
	 */
	public function find() : int
	{
		$found = $this->client->by_email( $this->subscription->getEmail() );
		if( is_object( $found )  && isset( $found->subscribers ) ){
			foreach ( $found->subscribers as $subscriber ){
				if( isset( $subscriber->email_address ) && $subscriber->email_address === $this->subscription->getEmail()  ){
					return $subscriber->id;
				}
			}
		}

		return 0;

	}

	/**
	 * Update on remote API
	 *
	 * @param int $ckId
	 *
	 * @return object|string
	 */
	public function update( int $ckId )
	{
		return $this->client->update($ckId, $this->subscriberData() );

	}

}