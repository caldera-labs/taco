<?php


namespace calderawp\taco\ConvertKit;
use calderawp\taco\edd\Subscription;


/**
 * Class Logic
 * @package calderawp\taco\ConvertKit
 */
class Logic {

	/**
	 * Add subscriber to the tag for their plan
	 *
	 * @param Subscription $subscription
	 */
	public static function add( Subscription $subscription )
	{
		$add = new Add( $subscription );
		$add->makeRequest();
	}

	/**
	 * Remove subscriber from the tag for their plan
	 *
	 * @param Subscription $subscription
	 */
	public static function remove( Subscription $subscription )
	{
		$remove = new Remove( $subscription );
		$remove->makeRequest();

	}

}