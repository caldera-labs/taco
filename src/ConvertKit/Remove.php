<?php


namespace calderawp\taco\ConvertKit;


/**
 * Class Remove
 * @package calderawp\taco\Subscribe
 */
class Remove extends Tagger {

	/**
	 * Remove all tags
	 */
	public function makeRequest()
	{

		$ckSubscriber = $this->findSubscriber();
		if ( ! $ckSubscriber ) {
			return;
		}

		foreach ( $this->ck_tags as $tag ) {
			$r = $this->client->unsubscribe( $tag, $ckSubscriber->id );
		}
	}

}