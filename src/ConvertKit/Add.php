<?php


namespace calderawp\taco\ConvertKit;


/**
 * Class Add
 * @package calderawp\taco\Subscribe
 */
class Add  extends Tagger {


	/**
	 * Add subscribers to the right tag
	 */
	public function makeRequest()
	{
		$this->client->subscribe( $this->findTag(), $this->subscription->getEmail(), $this->subscriberData() );

	}

}