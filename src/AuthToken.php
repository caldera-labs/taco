<?php


namespace calderawp\taco;


/**
 * Class AuthToken
 * @package calderawp\taco
 */
class AuthToken {

	protected static $sessionKey = '_taco_auth_token';
	public static function set( string  $token )
	{

		if ( class_exists( "\\WP_Session" ) ) {
			\WP_Session::get_instance()->offsetSet( self::$sessionKey, $token );

			return;
		}elseif ( function_exists( "EDD" ) ){
			EDD()->session->set( self::$sessionKey, $token );
			return;
		}

		static::maybe_start();
		$_SESSION[ self::$sessionKey ] = $token;
	}

	public static function get( )
	{

		if ( class_exists( "\\WP_Session" ) ) {
			return \WP_Session::get_instance()->offsetGet( self::$sessionKey );
		}elseif ( function_exists( "EDD" ) ){
			return EDD()->session->get( self::$sessionKey );
		}
		static::maybe_start();
		if ( isset( $_SESSION[ self::$sessionKey ] ) ) {
			return $_SESSION[ self::$sessionKey ];
		}
	}

	protected static function maybe_start(){
		if ( session_status() == PHP_SESSION_NONE ) {
			session_start();
		}
	}

}