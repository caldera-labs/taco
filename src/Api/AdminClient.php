<?php


namespace calderawp\taco\Api;
use calderawp\taco\Container;
use calderawp\taco\Entity\Account;


/**
 * Class AdminClient
 * @package calderawp\taco\Api
 */
class AdminClient {

	/** @var string  */
	protected $apiKey;

	public function __construct( string $apiKey )
	{

		$this->apiKey = $apiKey;
	}

	/**
	 * Find by WordPress subscription ID
	 *
	 * @param int $wpId
	 *
	 * @return Account|\WP_Error
	 */
	public function findByWpId( int $wpId )
	{

		$response = $this->makeRequest( '/account/find/' . $wpId );
		return $this->handleResponse( $response );

	}

	public function createAccount( int $wpId, string $email, string $name, string $plan )
	{
		$response = $this->makeRequest( '/account',[
			'wp_id'         => $wpId,
			'default_reply' => [
				'email' => $email,
				'name'  => $name
			],
			'plan' => $plan
		], 'PUT' );
		return $this->handleResponse( $response );
	}

	public function getAccount( int $id )
	{

		$response = $this->makeRequest( '/account/' . $id );
		return $this->handleResponse( $response );

	}

	public function updateAccountStatus( int $id, bool $active )
	{
		$response = $this->makeRequest( '/account/' . $id, [
			'active' => false
		], 'POST' );
		return $this->handleResponse( $response );


	}

	protected function handleResponse( $response, $goodCode = 200 )
	{
		if( ! is_wp_error( $response ) && $goodCode === wp_remote_retrieve_response_code( $response ) ){
			$body = json_decode( wp_remote_retrieve_body( $response ) );
			return new Account( $body );
		}elseif ( is_wp_error( $response ) ){
			return $response;
		}else{
			$body = json_decode( wp_remote_retrieve_body( $response ) );
			if( isset( $body->message ) ){
				$message = $body->message;
			}else{
				$message = "error";
			}
			return new \WP_Error( $message );
		}


	}

	protected function makeRequest( $endpoint, array $data = [], $method = 'GET' ){
		$url = Container::appUrl()  . $endpoint;
		$args = array(
			'headers' => array(
				'X-CS-ADMIN' => $this->apiKey,
				'content-type' => 'application/json'
			),
			'method' => $method
		);

		if( 'GET' == $method ){
			$url = add_query_arg( $data, $url );
		}else{
			$args[ 'body' ] = wp_json_encode( $data );
		}

		$request = wp_remote_request( $url, $args );
		return $request;
	}



}