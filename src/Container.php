<?php


namespace calderawp\taco;
use calderawp\taco\Api\AdminClient;



/**
 * Class Container
 * @package calderawp\taco
 */
class Container {

	/** @var  AdminClient */
	protected static $api;

	/** @var  \EDD_Subscriptions_DB */
	protected static $subDB;
	/**
	 * @return array
	 */
	public static function getConfig() : array
	{

		return apply_filters( 'taco_config', [
			'key'           => static::env( 'CS_ADMIN_KEY' ),
			'productId'     => static::env( 'CS_PRODUCT_ID' ),
			'apiUrl'        => static::env( 'CS_APP_URL', 'https://app.calderaformspro.com' ),
			'adminKey'      => static::env( 'CS_ADMIN_KEY' ),
			'ck_public'     => static::env( 'CK_PUBLIC' ),
			'ck_secret'     => static::env( 'CK_SECRET' ),
			'bundle_dev'       => (int) self::env( 'BUN_DEV', 20520 ),
			'bundle_agency'    => (int) self::env( 'BUN_AGENCY', 20518 ),
			'bundle_ent'       => (int) self::env( 'BUN_ENTERPRISE', 48255 )
		] );
	}

	/**
	 * Get environemnt value with fallback
	 *
	 * @param string $key Key to get from ENV
	 * @param null|mixed $default Optional, fallback value. If not set, null will be used as default
	 *
	 * @return null
	 */
	protected static function env( string $key, $default = null )
	{
		return isset( $_ENV[ $key] ) ? $_ENV[ $key ] : $default;
	}

	public static function getConfigItem( string $item )
	{
		if( array_key_exists( $item, self::getConfig()  ) ){
			return self::getConfig()[ $item ];
		}
	}

	/**
	 * Returns product ID for CF Pro
	 *
	 * IMPORTANT- Since bundles have CF Pro now, product ID can be space and not this ID. Use Logic::isSpace() to check.
	 *
	 * @return int
	 */
	public static function spaceId() :int
	{
		return intval( self::getConfigItem( 'productId' ) );
	}

	public static function proxyKey()
	{
		return self::getConfigItem( 'proxyRouteKey' );
	}

	/**
	 * @return AdminClient
	 */
	public static function getApiClient() : AdminClient
	{
		if( null === self::$api ){
			self::$api = new AdminClient( self::getConfigItem( 'adminKey'  ) );
		}

		return self::$api;

	}

	/**
	 * Get app URL
	 *
	 * @return string
	 */
	public static function appUrl() : string
	{
		if( function_exists( 'caldera_forms_pro_app_url' ) ){
			return caldera_forms_pro_app_url();
		}

		// THIS COPYPASTA IS DISPLEASING TO ME!!!!
		if( ! defined( 'CF_PRO_APP_URL' ) ) {
			define( 'CF_PRO_APP_URL', 'http://app.calderaformspro.com' );
		}
		return untrailingslashit( apply_filters( 'caldera_forms_pro_app_url', CF_PRO_APP_URL ) );
	}


	/**
	 * Get EDD subscriptions DB
	 *
	 * @return \EDD_Subscriptions_DB
	 */
	public static function getSubDB()
	{
		if( ! static::$subDB ){
			static::$subDB = new \EDD_Subscriptions_DB;
		}

		return static::$subDB;
	}

}