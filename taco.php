<?php
/**
 * Plugin Name: Taco
 * Version: 0.2.1
 */


define( 'TACO_VER', '0.2.1' );
add_action( 'plugins_loaded', function (){
	include_once __DIR__ . '/vendor/autoload.php';
	\calderawp\taco\Init::start();
}, 0 );
